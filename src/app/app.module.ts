import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovielistComponent } from './components/movielist/movielist.component';
import { MoviedetailsComponent } from './components/moviedetails/moviedetails.component';
import {HttpClientModule} from '@angular/common/http'
import {AccordionModule} from 'primeng/accordion'
@NgModule({
  declarations: [
    AppComponent,
    MovielistComponent,
    MoviedetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
HttpClientModule,
AccordionModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
