import { Injectable } from '@angular/core';
import {Observable } from 'rxjs'
import {map} from 'rxjs/operators'
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MoviedetailsService {

  constructor(private http:HttpClient) { }
  public getMovieDetails(input):Observable<any>{
    const url='https://api.themoviedb.org/3/movie/'+input+'?api_key=68e82445c8842ba8616d0f6bf0e97a41';
    return this.http.get(url).pipe(map(res=>res,err=>{
      throw err;
    }))
      }
}
