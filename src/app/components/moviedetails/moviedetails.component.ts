import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviedetailsService } from './moviedetails.service';

@Component({
  selector: 'app-moviedetails',
  templateUrl: './moviedetails.component.html',
  styleUrls: ['./moviedetails.component.scss']
})
export class MoviedetailsComponent implements OnInit {
  MovieInfo: any;
  url: string;

  constructor(private activatedRoute:ActivatedRoute,private moviedetailsvc:MoviedetailsService) {
    this.activatedRoute.params.subscribe(params=>{
      if(params['id']){
       this.getMovieDetails(params['id'])
      }
    })
   }

  ngOnInit(): void {
    this.url=localStorage.getItem('baseurl')
    this.url=this.url+'w300'
  }
getMovieDetails(id){
this.moviedetailsvc.getMovieDetails(id).subscribe(res=>{
  this.MovieInfo=res
})
}
}
