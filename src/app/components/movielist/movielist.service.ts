import { Injectable } from '@angular/core';
import {Observable } from 'rxjs'
import {map} from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MovielistService {
  headers: HttpHeaders;

  constructor(private http:HttpClient) { 

  }
  public getGenerList():Observable<any>{
    this.headers=new HttpHeaders().set('Content-Type',"application/json")
const url='https://api.themoviedb.org/3/genre/movie/list?api_key=68e82445c8842ba8616d0f6bf0e97a41';
return this.http.get(url).pipe(map(res=>res,err=>{
  throw err;
}))
  }
 public getMoviesOfGener(input):Observable<any>{
  this.headers=new HttpHeaders().set('Content-Type',"application/json")
  const url='https://api.themoviedb.org/3/genre/'+input+'/movies?api_key=68e82445c8842ba8616d0f6bf0e97a41';
  return this.http.get(url).pipe(map(res=>res,err=>{
    throw err;
  }))
    }
    
    public getConfig():Observable<any>{
      
      this.headers=new HttpHeaders().set('Content-Type',"application/json")
      const url='https://api.themoviedb.org/3/configuration?api_key=68e82445c8842ba8616d0f6bf0e97a41'
      
      return this.http.get(url).pipe(map(res=>res,err=>{
        throw err;
      }))
    }
}
