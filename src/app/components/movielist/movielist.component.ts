import { Component, OnInit } from '@angular/core';
import { MoviedetailsService } from '../moviedetails/moviedetails.service';
import { MovielistService } from './movielist.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movielist',
  templateUrl: './movielist.component.html',
  styleUrls: ['./movielist.component.scss']
})
export class MovielistComponent implements OnInit {
  generListArr: any[] = [];
  List: any[][];
  movies: unknown;
  alldetails: any=[];
  url: string;

  constructor(private movielistsvc: MovielistService,private router:Router) { }

  ngOnInit(): void {
    this.getConfigration()
    this.getGenerList()
  }
  getGenerList() {
    this.movielistsvc.getGenerList().subscribe(async res => {
      debugger
      this.generListArr = res.genres
      debugger
      for (let i = 0; i < this.generListArr.length; i++) {
        debugger
        this.movies= await this.getMoviesOfSelectedGener(this.generListArr[i].id)

        this.alldetails.push({
          gener:this.generListArr[i].name,
          movies:this.movies
        })
      }

      console.log(this.alldetails)
    })

  }
  getMoviesOfSelectedGener(id) {
    return new Promise(resolve => {
      this.movielistsvc.getMoviesOfGener(id).subscribe(res => {
        if (res) {
          resolve(res.results)
        }
      })
    })
  }
  onclick(id){
this.router.navigate(['movie',id])
  }
  getConfigration(){
    this.movielistsvc.getConfig().subscribe(res=>{
      debugger
      let configdata=res
      this.url=configdata.images.base_url+'w92'
      localStorage.setItem('baseurl',configdata.images.base_url)

    })
  }
}
